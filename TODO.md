# TODO
* ~~Finish Cursor methods~~
* ~~Add missing docstrings~~
* ~~Finish Connection methods~~
* ~~Write README~~
* ~~Write unit tests~~
* ~~Implement packaging support~~
* Improve README
* ~~Figure out a better strategy for loading libOdb.so~~
* Look into callproc
* Look at [Type Objects and Constructors](https://www.python.org/dev/peps/pep-0249/#type-objects-and-constructors)
* Look into auto-commit
* Figure out why BITFIELD doesn't work in Travis
